# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import View


class LoginView(View):

    def get(self, *args, **kwargs):
        return render(self.request, 'sahad/index.html')

    def post(self, *args, **kwargs):
        invitee = self.request.POST['invitee']
        print()
        if invitee == 'sahad':
            return render(self.request, 'sahad/index.html')
        elif invitee == 'sanoop':
            return render(self.request, 'sanoop/index.html')
        else:
            return render(self.request, 'login.html', {'error': 'wrong!!'})
